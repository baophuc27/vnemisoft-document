# Public API Endpoints

## Table of contents


## List of APIs

### Retrieve a node

Retrieve a node’s information by its **custom_id** or **original unique id**.

#### Arguments
1. attr: target id that need to be retrieved. (id, custom_id)

#### Request
```bash
GET /api/v1/nodes/:id
```

#### Example

```
GET /api/v1/nodes/P@DoMan#3
```

#### Response
```json
{
    "_id": "123",
    "createdTime": "2014-05-06T06:45:00.061Z",
    "isActive": true,
    "name": "My Workspace",
    "ownerId": "52969365593a1a3a3200000f",
    "customId": "workspace-01"
}
```

### Retrieve all nodes

Retrieve a list of all nodes available to the authenticated API key

#### Arguments
1. is_active: Filter if a node is active or not

#### Request
```bash
GET /api/v1/nodes
```

#### Example

```
GET /api/v1/nodes
```

#### Response
```json
[
    {
        "_id": "123",
        "createdTime": "2014-05-06T06:45:00.061Z",
        "isActive": true,
        "name": "My Workspace",
        "ownerId": "52969365593a1a3a3200000f",
        "customId": "workspace-01"
    },
    {
        "_id": "122",
        "createdTime": "2014-05-06T06:45:00.061Z",
        "isActive": true,
        "name": "My Workspace 2",
        "ownerId": "52969365593a1a3a3200000g",
        "customId": "workspace-02"
    }
]
```

### Retrieve node historic data

Retrieve a node historic data by its **id** or **custom_id**. Data can be returned in *JSON* or *CSV* format.

#### Arguments

|Argument | Description   	|
|---	|---	|
|format   	| Optional - Default is JSON. Data format to return: [JSON, CSV]   	|
|startTime	| Required. ISO8601 timestamp format, example: **2014-05-06T06:45:00.061Z**  	|
|endTime   	| Required. ISO8601 timestamp format, example: **2014-05-06T06:45:00.061Z**   	|
|limit	| Optional. Maximum number of historic records to be returned.	|
|aggregate	| Optional - Default is NONE (raw). Historic aggregate to apply to extracted data. [MIN,MAX,AVERAGE,MEDIAN,MEAN,INTERPOLATE]	|

#### Request

```
GET /api/v1/nodes/:id/historic?format=<>&limit=<>&startTime=<>&endTime=<>
```

#### Response
```json
{
    "docType": "json",
    "header": {
        "startTime": "2014-08-16T02:00:00.000Z",
        "endTime": "2014-08-16T02:20:43.000Z",
        "recordCount": 10,
        "columns": {
            "0": {
                "id": "132",
                "name": "Temperature",
                "dataType": "NUMBER",
                "aggregate": "NONE"
            },
            "1": {
                "id": "1332",
                "name": "Salinity",
                "dataType": "NUMBER",
                "aggregate": "AVERAGE"
            }
        }
    },
    "data": [
        {
            "ts": "2014-08-16T02:00:39.000Z",
            "f": { "0": {"v": 28.21 }
        },
        {
            "ts": "2014-08-16T02:05:40.000Z",
            "f": { "0": {"v": 28.22 } }
        },
        {
            "ts": "2014-08-16T02:10:41.000Z",
            "f": { "0": {"v": 28.7 } }
        },
        {
            "ts": "2014-08-16T02:15:42.000Z",
            "f": { "0": {"v": 29.2 } }
        },
        {
            "ts": "2014-08-16T02:20:43.000Z",
            "f": { "1": {"v": 29.18 } }
        },
        {
            "ts": "2014-08-16T02:00:39.000Z",
            "f": { "1": {"v": 28.21 }
        },
        {
            "ts": "2014-08-16T02:05:40.000Z",
            "f": { "1": {"v": 28.22 } }
        },
        {
            "ts": "2014-08-16T02:10:41.000Z",
            "f": { "1": {"v": 28.7 } }
        },
        {
            "ts": "2014-08-16T02:15:42.000Z",
            "f": { "0": {"v": 29.2 } }
        },
        {
            "ts": "2014-08-16T02:20:43.000Z",
            "f": { "1": {"v": 29.18 } }
        }
    ]
}
```

### Retrieve node future data

Retrieve a node future data by its **id** or **custom_id**. Data can be returned in *JSON* or *CSV* format.

#### Arguments

|Argument | Description   	|
|---	|---	|
|format   	| Optional - Default is JSON. Data format to return: [JSON, CSV]   	|
|startTime	| Required. ISO8601 timestamp format, example: **2014-05-06T06:45:00.061Z**  	|
|endTime   	| Required. ISO8601 timestamp format, example: **2014-05-06T06:45:00.061Z**   	|
|limit	| Optional. Maximum number of historic records to be returned.	|

#### Request

```
GET /api/v1/nodes/:id/future?format=<>&limit=<>&startTime=<>&endTime=<>
```

#### Response
```json
{
    "docType": "json",
    "header": {
        "startTime": "2014-08-16T02:00:00.000Z",
        "endTime": "2014-08-16T02:20:43.000Z",
        "recordCount": 10,
        "columns": {
            "0": {
                "id": "132",
                "name": "Temperature",
                "dataType": "NUMBER",
                "aggregate": "NONE"
            },
            "1": {
                "id": "1332",
                "name": "Salinity",
                "dataType": "NUMBER",
                "aggregate": "AVERAGE"
            }
        }
    },
    "data": [
        {
            "ts": "2014-08-16T02:00:39.000Z",
            "f": { "0": {"v": 28.21 }
        },
        {
            "ts": "2014-08-16T02:05:40.000Z",
            "f": { "0": {"v": 28.22 } }
        },
        {
            "ts": "2014-08-16T02:10:41.000Z",
            "f": { "0": {"v": 28.7 } }
        },
        {
            "ts": "2014-08-16T02:15:42.000Z",
            "f": { "0": {"v": 29.2 } }
        },
        {
            "ts": "2014-08-16T02:20:43.000Z",
            "f": { "1": {"v": 29.18 } }
        },
        {
            "ts": "2014-08-16T02:00:39.000Z",
            "f": { "1": {"v": 28.21 }
        },
        {
            "ts": "2014-08-16T02:05:40.000Z",
            "f": { "1": {"v": 28.22 } }
        },
        {
            "ts": "2014-08-16T02:10:41.000Z",
            "f": { "1": {"v": 28.7 } }
        },
        {
            "ts": "2014-08-16T02:15:42.000Z",
            "f": { "0": {"v": 29.2 } }
        },
        {
            "ts": "2014-08-16T02:20:43.000Z",
            "f": { "1": {"v": 29.18 } }
        }
    ]
}
```

### Retrieve a device

Retrieve a device by its **id** or **custom_id**.

#### Arguments

|Argument | Description   	|
|---	|---	|
|id   	| Required - Device's **id** or **custom_id** 	|

#### Request

```
GET /api/v1/devices/:id
```
#### Response
```json
{
    "_id": "123",
    "createdTime": "2014-05-06T06:45:00.061Z",
    "isActive": true,
    "name": "My Workspace",
    "ownerId": "52969365593a1a3a3200000f",
    "customId": "@workspace-01",
    "isConnected": true,
}
```


